CREATE TABLE database_change_log(
                                    id INT NOT NULL auto_increment PRIMARY KEY,
                                    action_type VARCHAR(1),
                                    start_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    end_date DATETIME NOT NULL ,
                                    user_id VARCHAR(100),
                                    script_name VARCHAR(100) UNIQUE,
                                    status VARCHAR(50),
                                    app_version VARCHAR (30));