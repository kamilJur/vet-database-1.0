DROP TABLE IF EXISTS ANIMAL_OWNER;
CREATE TABLE ANIMAL_OWNER(
                             ID INT AUTO_INCREMENT PRIMARY KEY,
                             NAME VARCHAR(20),
                             PHONE VARCHAR(12),
                             ADDRESS VARCHAR(50)
);

DROP TABLE IF EXISTS VETERINARIAN;
CREATE TABLE VETERINARIAN(
                             ID INT AUTO_INCREMENT PRIMARY KEY,
                             NAME VARCHAR (20),
                             PHONE VARCHAR(12)
);

DROP TABLE IF EXISTS ANIMALS;
CREATE TABLE ANIMALS(
                        ID INT AUTO_INCREMENT PRIMARY KEY,
                        VETERINARIAN_ID INT,
                        ANIMAL_OWNER_ID INT,
                        NAME VARCHAR(20),
                        SEX VARCHAR(1),
                        TYPE VARCHAR(20),
                        COLOR VARCHAR(20),
                        BIRTH_DATE DATE,
                        LAST_VISIT_DATE DATE,
                        LAST_GRAFTING_DATE DATE,
                        DESCRIPTION VARCHAR (200),
                        FOREIGN KEY ( ANIMAL_OWNER_ID) REFERENCES ANIMAL_OWNER(ID),
                        FOREIGN KEY (VETERINARIAN_ID) REFERENCES VETERINARIAN(ID)
);

DROP TABLE IF EXISTS SERVICES;
CREATE TABLE SERVICES(
                         ID INT PRIMARY KEY,
                         NAME VARCHAR(30),
                         ANIMAL_ID INT,
                         PRICE DECIMAL(6,2),
                         DATE DATE,
                         FOREIGN KEY (ANIMAL_ID)  REFERENCES ANIMALS(ID)
);

