DROP TABLE data_history;

CREATE TABLE database_change_log(
                             id INT NOT NULL auto_increment PRIMARY KEY,
                             action_type VARCHAR(1),
                             start_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             end_date DATETIME NOT NULL ,
                             user_id VARCHAR(100),
                             script_name VARCHAR(100) UNIQUE,
                             status VARCHAR(50),
                             app_version VARCHAR (30));
DELIMITER //
CREATE PROCEDURE script_start(IN n_script_name VARCHAR(100) , IN n_action_type VARCHAR(1) , IN n_user_id VARCHAR(100) ,  IN n_app_version VARCHAR(30))
BEGIN
    INSERT INTO data_history(script_name, action_type, user_id, app_version, status) VALUES
    (n_script_name, n_action_type, n_user_id, n_app_version, "in-progress");
END
//
DELIMITER ;

CALL script_start('schema-change-1.0-10032020','I','Klaudia Sadło','1.0');
DROP PROCEDURE script_start;


DELIMITER //
CREATE PROCEDURE script_end(IN n_script_name VARCHAR(100))
BEGIN
    UPDATE data_history
    SET end_date = CURRENT_TIMESTAMP,
        status = 'Completed'
    WHERE n_script_name = script_name;
END
//
DELIMITER ;

DROP PROCEDURE script_end;
CALL script_end('schema-change-1.0-10032020');
SELECT * FROM data_history;


