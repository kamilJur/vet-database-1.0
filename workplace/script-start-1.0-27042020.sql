DELIMITER //
CREATE PROCEDURE script_start(IN n_script_name VARCHAR(100) , IN n_action_type VARCHAR(1) , IN n_user_id VARCHAR(100) ,  IN n_app_version VARCHAR(30))
BEGIN
    INSERT INTO database_change_log(script_name, action_type, user_id, app_version, status) VALUES
    (n_script_name, n_action_type, n_user_id, n_app_version, "In-progress");
END
//
DELIMITER ;