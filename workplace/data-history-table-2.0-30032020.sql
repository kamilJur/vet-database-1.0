CREATE TABLE data_history(
	LOGID INT NOT NULL auto_increment PRIMARY KEY,
	ID int,
	ACTIONTYPE varchar(10),
	TIMEDATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	USERLOGIN varchar(50),
	VETERINARIAN_ID INT,
	ANIMAL_OWNER_ID INT,
	NAME VARCHAR(20),
	SEX VARCHAR(1),
	TYPE VARCHAR(20),
	COLOR VARCHAR(20),
	BIRTH_DATE DATE,
	LAST_VISIT_DATE DATE,
	LAST_GRAFTING_DATE DATE,
	DESCRIPTION VARCHAR (200),
	FOREIGN KEY ( ANIMAL_OWNER_ID) REFERENCES ANIMAL_OWNER(ID),
	FOREIGN KEY (VETERINARIAN_ID) REFERENCES veterinarian(ID)
);

DROP TABLE data_history;
DROP TRIGGER IF EXISTS data_insert;

CREATE TRIGGER data_insert AFTER INSERT ON Animals FOR EACH ROW
    INSERT INTO data_history(ID,ACTIONTYPE, TIMEDATE,USERLOGIN, VETERINARIAN_ID ,ANIMAL_OWNER_ID ,NAME ,SEX,TYPE ,COLOR ,BIRTH_DATE,LAST_VISIT_DATE ,LAST_GRAFTING_DATE,DESCRIPTION)
		VALUES(
						new.id, 
						'Insert', 
						now(),
						current_user(),
						new.VETERINARIAN_ID ,
                        new.ANIMAL_OWNER_ID ,
                        new.NAME ,
                        new.SEX,
                        new.TYPE ,
                        new.COLOR ,
                        new.BIRTH_DATE,
                        new.LAST_VISIT_DATE ,
                        new.LAST_GRAFTING_DATE ,
                        new.DESCRIPTION );
    




select * from data_history;