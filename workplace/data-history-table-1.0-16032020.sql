CREATE TABLE data_history LIKE Animals;
DROP TABLE data_history;

ALTER TABLE data_history MODIFY COLUMN ID int(11) NOT NULL, 
   DROP PRIMARY KEY, ENGINE = MyISAM, ADD action VARCHAR(8) DEFAULT 'insert' FIRST, 
   ADD revision INT(6) NOT NULL AUTO_INCREMENT AFTER action,
   ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
   ADD PRIMARY KEY (ID, revision);

DROP TRIGGER IF EXISTS data_insert;
DROP TRIGGER IF EXISTS data_update;
DROP TRIGGER IF EXISTS data_delete;

CREATE TRIGGER data_insert AFTER INSERT ON Animals FOR EACH ROW
    INSERT INTO data_history SELECT 'insert', NULL, NOW(), d.* 
    FROM Animals AS d WHERE d.ID = NEW.ID;

CREATE TRIGGER data_update AFTER UPDATE ON Animals FOR EACH ROW
    INSERT INTO data_history SELECT 'update', NULL, NOW(), d.*
    FROM Animals AS d WHERE d.ID = NEW.ID;

CREATE TRIGGER data_delete BEFORE DELETE ON Animals FOR EACH ROW
    INSERT INTO data_history SELECT 'delete', NULL, NOW(), d.* 
    FROM Animals AS d WHERE d.ID = OLD.ID;
    
select * from data_history;