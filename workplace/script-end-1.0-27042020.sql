DELIMITER //
CREATE PROCEDURE script_end(IN n_script_name VARCHAR(100))
BEGIN
    UPDATE database_change_log
    SET end_date = CURRENT_TIMESTAMP,
        status = 'Completed'
    WHERE n_script_name = script_name;
END
//
DELIMITER ;